#include <pybind11/pybind11.h>
#include <pybind11/stl.h> // for pybinding vectors
#include <pybind11/functional.h> // for pybinding functions

#include <memory>

#include "particle/particle.hpp"
#include "ensemble/ensemble.hpp"
#include "utils/vectorutils.hpp"


namespace py = pybind11;

PYBIND11_MODULE(_md, module) {

    using namespace pybind11::literals;
    
    py::class_<Particle, std::shared_ptr<Particle>>(module, "Particle")
        .def(py::init<vectorutils::realvector2d, vectorutils::realvector2d, double const, double const>(), 
             "position"_a, "velocity"_a, "dt"_a, "L"_a)
        .def("step", &Particle::step, "force"_a, "relaxation"_a=false)
        .def("relative_vec_to", &Particle::relative_vec_to, "other"_a)
        .def("force_on_me", &Particle::force_on_me, "other"_a)
        .def("set_velocity", &Particle::set_velocity, "velocity"_a)
        .def("get_velocity", &Particle::get_velocity)
        .def_readonly("position", &Particle::position);

    py::class_<Ensemble, std::shared_ptr<Ensemble>>(module, "Ensemble")
        .def(py::init<int, double, double, unsigned long>(), "N"_a, "rho"_a, "dt"_a, "seed"_a=0)
        .def("step", &Ensemble::step, "relaxation"_a=false, "steps"_a=1)
        .def("relaxate", &Ensemble::relaxate, "epsilon_convergence"_a=1e-6, "max_steps"_a=500000)
        .def("pressure", &Ensemble::pressure, "steps"_a=1000)
        .def("potential_energy", &Ensemble::potential_energy)
        .def("kinetic_energy", &Ensemble::kinetic_energy)
        .def("total_energy", &Ensemble::total_energy)
        .def("average_velocity", &Ensemble::average_velocity)
        .def("temperature", &Ensemble::temperature, "steps"_a=1000)
        .def("random_velocities", &Ensemble::random_velocities, "kinetic_energy"_a, "seed"_a)
        .def_readonly("N", &Ensemble::N)
        .def_readonly("rho", &Ensemble::rho)
        .def_readonly("L", &Ensemble::L)
        .def_readonly("dt", &Ensemble::dt)
        .def_readonly("particles", &Ensemble::particles);

}
