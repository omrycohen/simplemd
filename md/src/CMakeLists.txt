add_subdirectory(particle)
add_subdirectory(ensemble)
add_subdirectory(interaction)

include_directories("${CMAKE_CURRENT_LIST_DIR}")

pybind11_add_module(_md _md.cpp
                        particle/particle.cpp
                        ensemble/ensemble.cpp
                        interaction/interaction.cpp)
target_link_libraries(_md PRIVATE pybind11::module)

pybind11_add_module(interaction interaction.cpp 
                                interaction/interaction.cpp)
target_link_libraries(interaction PRIVATE pybind11::module)