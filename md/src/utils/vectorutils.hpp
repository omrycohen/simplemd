#ifndef VECTORUTILS_HPP
#define VECTORUTILS_HPP

#include <array>
#include <random>

namespace vectorutils{

    using realvector2d = std::array<double, 2>;
   
}

#endif // VECTORUTILS_HPP
