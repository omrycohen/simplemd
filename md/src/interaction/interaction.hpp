#ifndef INTERACTION_HPP
#define INTERACTION_HPP

namespace interaction{

    double potential(double);
    double force_magnitude(double);
    
}

#endif // INTERACTION_HPP