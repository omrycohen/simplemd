#include <cmath>
#include <random>
#include <stdexcept>
#include <sstream>

#include "ensemble.hpp"
#include "utils/vectorutils.hpp"
#include "interaction/interaction.hpp"

Ensemble::Ensemble(int N_, double rho_, double dt_, unsigned long seed): 
N(N_), rho(rho_), L(std::sqrt(N/rho)), dt(dt_), forces(N, {0., 0.}) {

    std::default_random_engine generator;
    generator.seed(seed);
    std::uniform_real_distribution<double> distribution(0., L);
    
    particles.reserve(N);
    for(int i=0; i<N; ++i){
        particles.emplace_back(vectorutils::realvector2d({distribution(generator), distribution(generator)}), 
                               vectorutils::realvector2d({0., 0.}), 
                               dt, 
                               L);
    }
    update_forces();
}


Ensemble Ensemble::step(bool relaxation=false, int steps=1){
    
    for (int counter=0; counter<steps; ++counter){
         for (int i=0; i<N; ++i) particles[i].step(forces[i], relaxation);
        update_forces();
    }
    return *this;
}


Ensemble Ensemble::relaxate(double epsilon_convergence, int max_steps){

    bool converged = false;
    int num_of_steps = 100;

    // do `num_of_steps` steps to initialize motion
    step(true, num_of_steps);

    while (!converged) {
        step(true);
        converged = true;
        for (Particle const& particle : particles){
            vectorutils::realvector2d const velocity = particle.get_velocity();
            double const norm2 = velocity[0]*velocity[0] + velocity[1]*velocity[1];
            bool const moved_too_much = (norm2 > epsilon_convergence*epsilon_convergence);

            if (moved_too_much) {   
                converged = false;
            }
        }
        ++num_of_steps;
        if (num_of_steps >= max_steps){ 
            std::stringstream ss;
            ss << "Maximum number of steps reached: " << num_of_steps;
            throw std::runtime_error(ss.str());
        }
    }
    return *this;
}


double Ensemble::pressure(int steps){

    double p = 0.;

    for (int iter=0; iter<steps; ++iter){
        step();
        for (int i=0; i<N; ++i){
            vectorutils::realvector2d const velocity = particles[i].get_velocity();
            p += std::pow(velocity[0], 2) + std::pow(velocity[1], 2);
            for (int j=i+1; j<N; ++j){
                vectorutils::realvector2d const rel_vec = particles[i].relative_vec_to(particles[j]);
                vectorutils::realvector2d const force = particles[j].force_on_me(particles[i]);
                p += rel_vec[0]*force[0] + rel_vec[1]*force[1];
            }
        }
    }
    
    return p/steps/2./L/L;
}


double Ensemble::potential_energy(){
    double e = 0.;
    for (int i=0; i<N; ++i){
        for (int j=i+1; j<N; ++j){
            vectorutils::realvector2d const rel_vec = particles[i].relative_vec_to(particles[j]);
            double const dr = std::sqrt(std::pow(rel_vec[0], 2) + std::pow(rel_vec[1], 2));
            e += interaction::potential(dr);
        }
    }
    return e;
}


double Ensemble::kinetic_energy(bool relative_to_center_of_mass = false){
    double e = 0.;
    vectorutils::realvector2d const average_velocity_ = 
        relative_to_center_of_mass ? average_velocity() : vectorutils::realvector2d({0., 0.});
    for (int i=0; i<N; ++i){
        vectorutils::realvector2d const velocity = particles[i].get_velocity();
        e += 0.5 * (std::pow(velocity[0]-average_velocity_[0], 2) + std::pow(velocity[1]-average_velocity_[1], 2));
    }
    return e;
}


vectorutils::realvector2d Ensemble::average_velocity(){
    vectorutils::realvector2d average_velocity_({0., 0.});
    for (int i=0; i<N; ++i){
        vectorutils::realvector2d const velocity = particles[i].get_velocity();
        average_velocity_[0] += velocity[0];
        average_velocity_[1] += velocity[1];
    }
    average_velocity_[0] /= N; 
    average_velocity_[1] /= N; 
    return average_velocity_;
}


double Ensemble::total_energy(){
    return potential_energy() + kinetic_energy();
}


double Ensemble::temperature(int steps){
    
    double t = 0;
    
    for (int i=0; i<steps; ++i){

        step();
        
        t += kinetic_energy(true);
    }

    return t/steps/N;
}


void Ensemble::random_velocities(double kinetic_energy_, unsigned long seed){

    if (kinetic_energy_ == 0.){
        for(int i=0; i<N; ++i){
            particles[i].set_velocity(vectorutils::realvector2d({0., 0.}));
        }
        return;
    }

    std::default_random_engine generator;
    generator.seed(seed);
    std::normal_distribution<double> distribution(0., kinetic_energy_/N); 
    
    // generate random velocities
    for(int i=0; i<N; ++i){
        particles[i].set_velocity(vectorutils::realvector2d({distribution(generator), distribution(generator)}));
    }

    // make sure that the average velocity vanishes
    vectorutils::realvector2d const average_velocity_ = average_velocity();
    for(int i=0; i<N; ++i){
        vectorutils::realvector2d const velocity = particles[i].get_velocity();
        particles[i].set_velocity(vectorutils::realvector2d({
            velocity[0] - average_velocity_[0],
            velocity[1] - average_velocity_[1]
        }));
    }

    // normalize the energy
    double const factor = std::sqrt(kinetic_energy_ / kinetic_energy());
    for(int i=0; i<N; ++i){
        vectorutils::realvector2d const velocity = particles[i].get_velocity();
        particles[i].set_velocity(vectorutils::realvector2d({
            velocity[0] * factor,
            velocity[1] * factor
        }));
    }
}


void Ensemble::update_forces(){

    // make all forces zero
    for (auto& force : forces){ force = vectorutils::realvector2d({0., 0.});}

    for (int i=0; i<N; ++i){
        for (int j=i+1; j<N; ++j){
            
            auto const force_on_i_by_j = particles[i].force_on_me(particles[j]);

            forces[i][0] += force_on_i_by_j[0];
            forces[i][1] += force_on_i_by_j[1];

            forces[j][0] -= force_on_i_by_j[0];
            forces[j][1] -= force_on_i_by_j[1];

        }
    }
}
