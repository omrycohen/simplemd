#ifndef ENSEMBLE_HPP
#define ENSEMBLE_HPP

#include "particle/particle.hpp"
#include "utils/vectorutils.hpp"
#include <vector>

struct Ensemble{

    // attributes
    public:

    int const N;
    double const rho;
    double const L;
    std::vector<Particle> particles;
    double const dt;

    // auxiliary variables
    std::vector<vectorutils::realvector2d> forces;

    // methods
    Ensemble(int, double, double, unsigned long);
    Ensemble step(bool, int);
    Ensemble relaxate(double, int);
    double pressure(int);
    
    double potential_energy();
    double kinetic_energy(bool);
    double total_energy();

    vectorutils::realvector2d average_velocity();

    double temperature(int);

    void random_velocities(double, unsigned long);

    void update_forces();

};

#endif // ENSEMBLE_HPP