#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include <vector>
#include <functional>

#include "utils/vectorutils.hpp"

struct Particle{

    private:

    vectorutils::realvector2d velocity;

    public:

    // attributes
    vectorutils::realvector2d position;
    vectorutils::realvector2d position_old;
    vectorutils::realvector2d dposition_old;
    double const L;
    double const dt;

    // methods
    Particle(vectorutils::realvector2d, vectorutils::realvector2d, double const, double const);
    Particle step(vectorutils::realvector2d, bool);
    vectorutils::realvector2d relative_vec_to(Particle const&);
    vectorutils::realvector2d force_on_me(Particle const&);
    void set_velocity(vectorutils::realvector2d velocity_);
    vectorutils::realvector2d const& get_velocity() const;
};

#endif // PARTICLE_HPP