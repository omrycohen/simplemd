#include "particle.hpp"
#include "interaction/interaction.hpp"
#include <cmath> 
#include <functional>


Particle::Particle(vectorutils::realvector2d position_, 
                   vectorutils::realvector2d velocity_, 
                   double const dt_,
                   double const L_):
                   position(position_),
                   L(L_),
                   dt(dt_){

    // BC handling
    while (position[0] < 0.) position[0] += L;
    while (position[0] >= L) position[0] -= L;
    while (position[1] < 0.) position[1] += L;
    while (position[1] >= L) position[1] -= L;
    
    set_velocity(velocity_);
}


Particle Particle::step(vectorutils::realvector2d force, bool relaxation){
    if (relaxation){
        position_old[0] = position[0];
        position_old[1] = position[1]; 
        dposition_old[0] = 0;
        dposition_old[1] = 0;
    }

    // update position
    position_old[0] = force[0]*dt*dt + position[0] + dposition_old[0];
    position_old[1] = force[1]*dt*dt + position[1] + dposition_old[1];
    // position_old[0] = force[0]*dt*dt + 2.*position[0] - position_old[0];
    // position_old[1] = force[1]*dt*dt + 2.*position[1] - position_old[1];
    std::swap(position, position_old);

    // calculate velocity
    velocity[0] = (position[0] - position_old[0] + dposition_old[0]) / 2./ dt;
    velocity[1] = (position[1] - position_old[1] + dposition_old[1]) / 2./ dt;
    
    // calculate position change BEFORE handling cyclic BC
    dposition_old[0] = position[0] - position_old[0];
    dposition_old[1] = position[1] - position_old[1];
    
    // BC handling
    while (position[0] < 0.) position[0] += L;
    while (position[0] >= L) position[0] -= L;
    while (position[1] < 0.) position[1] += L;
    while (position[1] >= L) position[1] -= L;

    return *this;

}


vectorutils::realvector2d Particle::relative_vec_to(Particle const& other){
    
    vectorutils::realvector2d rel_vec({0., 0.});
    
    rel_vec[0] = other.position[0] - position[0];
    rel_vec[1] = other.position[1] - position[1];

    // BC handling
    while (rel_vec[0] < -L/2.) rel_vec[0] += L;
    while (rel_vec[0] >= L/2.) rel_vec[0] -= L;
    while (rel_vec[1] < -L/2.) rel_vec[1] += L;
    while (rel_vec[1] >= L/2.) rel_vec[1] -= L;

    return rel_vec;
}


vectorutils::realvector2d Particle::force_on_me(Particle const& other){
    vectorutils::realvector2d const rel_vec = relative_vec_to(other);
    double const distance = std::sqrt(rel_vec[0]*rel_vec[0] + rel_vec[1]*rel_vec[1]);
    double const force_mag = interaction::force_magnitude(distance);
    return {- force_mag * rel_vec[0] / distance, - force_mag * rel_vec[1] / distance};
}


void Particle::set_velocity(vectorutils::realvector2d velocity_){

    velocity[0] = velocity_[0];
    velocity[1] = velocity_[1];

    dposition_old[0] = velocity[0] * dt;
    dposition_old[1] = velocity[1] * dt;

    position_old[0] = position[0] - dposition_old[0];
    position_old[1] = position[1] - dposition_old[1]; 

    // BC handling
    while (position_old[0] < 0.) position[0] += L;
    while (position_old[0] >= L) position[0] -= L;
    while (position_old[1] < 0.) position[1] += L;
    while (position_old[1] >= L) position[1] -= L;
}


vectorutils::realvector2d const& Particle::get_velocity() const{
    return velocity;
}
